// BossDialog.cpp: ���� ����������
//

#include "stdafx.h"
#include "Boss.h"
#include "BossDialog.h"
#include "afxdialogex.h"


// ���������� ���� CBossDialog

IMPLEMENT_DYNAMIC(CBossDialog, CDialog)

CBossDialog::CBossDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CBossDialog::IDD, pParent)
{
	m_interrupt = FALSE;
	m_killEvent = NULL;
}

CBossDialog::~CBossDialog()
{
}

void CBossDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDOUTPUT, m_output);
	DDX_Control(pDX, IDSTART, m_start);
	DDX_Control(pDX, IDSTOP, m_stop);
	DDX_Control(pDX, IDCANCEL, m_cancel);

	m_stop.EnableWindow(FALSE);
}


BEGIN_MESSAGE_MAP(CBossDialog, CDialog)
	ON_BN_CLICKED(IDSTART, &CBossDialog::OnBnClickedStart)
	ON_BN_CLICKED(IDSTOP, &CBossDialog::OnBnClickedStop)
	ON_BN_CLICKED(IDCANCEL, &CBossDialog::OnBnClickedCancel)
END_MESSAGE_MAP()


// ����������� ��������� CBossDialog


void CBossDialog::OnBnClickedStart()
{
	bool success = false;
	
	if (m_interrupt)
		m_interrupt = FALSE;

	m_start.EnableWindow(FALSE);
	m_stop.EnableWindow(TRUE);
	success = CreateWorkingThread();
	if (!success)
		AfxMessageBox(_T("Something goes wrong!"), IDB_INFO);
}

void CBossDialog::OnBnClickedStop()
{
	m_stop.EnableWindow(FALSE);
	m_start.EnableWindow(TRUE);
	m_interrupt = TRUE;
}

void CBossDialog::OnBnClickedCancel()
{
	if (m_thread && m_killEvent)
	{
		SetEvent(m_killEvent);
		if (WaitForSingleObject(m_thread, 1000) == WAIT_TIMEOUT)
		{
			// fail!
		}
	}

	CDialog::OnCancel();
}

bool CBossDialog::CreateWorkingThread()
{
	DWORD workingThreadId = 0;

	m_killEvent = CreateEvent(NULL, FALSE, FALSE, NULL);
	m_thread = CreateThread(NULL, 0, &CBossDialog::WorkingThread, (LPVOID)this, 0, &workingThreadId);
	return (m_thread) ? true : false;
}

DWORD WINAPI CBossDialog::WorkingThread(LPVOID param)
{
	ASSERT(param);
	CBossDialog* dialog = static_cast<CBossDialog*>(param);
	AFXASSUME(dialog);
	DWORD error = 0;
	
	error = static_cast<DWORD>(dialog->WorkingFoo());
	CloseHandle(dialog->m_thread);
	CloseHandle(dialog->m_killEvent);
	dialog->m_thread = NULL;
	dialog->m_killEvent = NULL;
	_endthreadex(error);
	return error;
}

DWORD CBossDialog::WorkingFoo()
{
	CString text;
	int i = 0;

	while (TRUE)
	{
		Sleep(250);
		if (m_interrupt) // stop button clicked
		{
			text.Format(_T("Work of function was interrupted at cycle iteration #%d\r\n"), ++i);
			AppendTextToOutput(text);
			break;
		}
		else if (WaitForSingleObject(m_killEvent, 0) == WAIT_OBJECT_0) // cancel button clicked
			break;
		else 
		{
			text.Format(_T("Cycle iteration #%d\r\n"), ++i);
			AppendTextToOutput(text);
		}
	}

	return 0;
}

void CBossDialog::AppendTextToOutput(CString& text)
{
	int length = 0;

	length = m_output.GetWindowTextLengthA();
	m_output.SetSel(length, length);
	m_output.ReplaceSel(text);
}