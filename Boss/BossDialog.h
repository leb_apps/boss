#pragma once
#include "afxwin.h"


// ���������� ���� CBossDialog

class CBossDialog : public CDialog
{
	DECLARE_DYNAMIC(CBossDialog)

public:
	CBossDialog(CWnd* pParent = NULL);   // ����������� �����������
	virtual ~CBossDialog();

// ������ ����������� ����
	enum { IDD = IDD_BOSSDIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // ��������� DDX/DDV

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedStart();
	afx_msg void OnBnClickedStop();
	afx_msg void OnBnClickedCancel();

private:
	bool CreateWorkingThread();
	static DWORD WINAPI WorkingThread(LPVOID param);
	DWORD WorkingFoo();
	void AppendTextToOutput(CString& text);

private:
	BOOL	m_interrupt;

	CEdit	m_output;
	CButton m_start;
	CButton m_stop;
	CButton m_cancel;

	HANDLE	m_thread;
	HANDLE m_killEvent;
};
