// ���� �������� ��� MFC Samples ������������� ���������������� ����������������� ���������� Fluent �� ������ MFC � Microsoft Office
// ("Fluent UI") � ��������������� ������������� ��� ���������� �������� � �������� ���������� �
// ����������� �� ������ Microsoft Foundation Classes � ��������� ����������� ������������,
// ���������� � ����������� ����������� ���������� MFC C++. 
// ������� ������������� ���������� �� �����������, ������������� ��� ��������������� Fluent UI �������� ��������. 
// ��� ��������� �������������� �������� � ����� ������������ ��������� Fluent UI �������� ���-����
// http://msdn.microsoft.com/officeui.
//
// (C) ���������� ���������� (Microsoft Corp.)
// ��� ����� ��������.

// BossView.cpp : ���������� ������ CBossView
//

#include "stdafx.h"
// SHARED_HANDLERS ����� ���������� � ������������ �������� ��������� ���������� ������� ATL, �������
// � ������; ��������� ��������� ������������ ��� ��������� � ������ �������.
#ifndef SHARED_HANDLERS
#include "Boss.h"
#endif

#include "BossDoc.h"
#include "BossView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CBossView

IMPLEMENT_DYNCREATE(CBossView, CView)

BEGIN_MESSAGE_MAP(CBossView, CView)
	ON_WM_CONTEXTMENU()
	ON_WM_RBUTTONUP()
	ON_COMMAND(ID_BUTTON_OPEN, &CBossView::OnButtonOpen)
END_MESSAGE_MAP()

// ��������/����������� CBossView

CBossView::CBossView()
{
	// TODO: �������� ��� ��������

}

CBossView::~CBossView()
{
}

BOOL CBossView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: �������� ����� Window ��� ����� ����������� ���������
	//  CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

// ��������� CBossView

void CBossView::OnDraw(CDC* /*pDC*/)
{
	CBossDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;

	// TODO: �������� ����� ��� ��������� ��� ����������� ������
}

void CBossView::OnRButtonUp(UINT /* nFlags */, CPoint point)
{
	ClientToScreen(&point);
	OnContextMenu(this, point);
}

void CBossView::OnContextMenu(CWnd* /* pWnd */, CPoint point)
{
#ifndef SHARED_HANDLERS
	theApp.GetContextMenuManager()->ShowPopupMenu(IDR_POPUP_EDIT, point.x, point.y, this, TRUE);
#endif
}


// ����������� CBossView

#ifdef _DEBUG
void CBossView::AssertValid() const
{
	CView::AssertValid();
}

void CBossView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CBossDoc* CBossView::GetDocument() const // �������� ������������ ������
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CBossDoc)));
	return (CBossDoc*)m_pDocument;
}
#endif //_DEBUG


// ����������� ��������� CBossView


void CBossView::OnButtonOpen()
{
	CBossDialog dialog;

	dialog.DoModal();
}
